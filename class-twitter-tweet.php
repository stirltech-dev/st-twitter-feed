<?php

namespace ST\Twitter;

class Tweet {

  /**
   * The tweet object in its entirety, from Twitter API response
   * @var object
   */
  protected $tweet;

  /**
   * The plain text of the tweet
   * @var string
   */
  public $text;

  /**
   * The filtered html text of the tweet with links for hashtags and usernames
   * @var string
   */
  public $html;

  /**
   * The link to the tweet or to the referenced link
   * @var string
   */
  public $link;

  /**
   * The URL to the user's avatar
   * @var string
   */
  public $avatar;

  /**
   * The screen name or handle of the Twitter user
   * @var string
   */
  public $handle;

  /**
   * The formatted time since the tweet was posted, as determined by the creation date
   * @var string
   */
  public $time_ago;

  /**
   * Build the defaults from the passed tweet object returned from Twitter's API
   * @param object $tweet_object The parsed JSON array from Twitter's API for a single Tweet/status
   */
  public function __construct( $tweet_object ) {
    $this->tweet = $tweet_object;
    $this->text = $tweet_object->text;
    $this->html = $this->build_html_from_text();
    $this->link = $this->build_link();
    $this->avatar = $tweet_object->user->profile_image_url;
    $this->handle = $tweet_object->screen_name;
    $this->time_ago = $this->build_timestamp();
  }

  /**
   * Parse the text of the tweet to create HTML links to usernames and for string links
   * @return string
   */
  protected function build_html_from_text() {
    $patterns = array('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', '/@([A-Za-z0-9_]{1,15})/');
    $replace = array('<a href="$1">$1</a>', '<a href="http://twitter.com/$1">@$1</a>');
    return preg_replace($patterns, $replace, $this->text);
  }

  /**
   * Build a link to the Tweet or, if a link was provided, to the first link mentioned
   * @return string
   */
  protected function build_link() {
    if ( $this->tweet->entities->urls ) {
      return $this->tweet->entities->urls[0]->url;
    }
    return sprintf( 'https://twitter.com/%s/status/%d', $this->username, $this->tweet->id );
  }

  /**
   * Build the formatted "time ago" type timestamp from the created at date
   * @return string
   */
  protected function build_timestamp() {
    //get current timestampt
    $b = strtotime("now");
    //get timestamp when tweet created
    $c = strtotime($this->tweet->created_at);
    //get difference
    $d = $b - $c;
    //calculate different time values
    $minute = 60;
    $hour = $minute * 60;
    $day = $hour * 24;
    $week = $day * 7;

    if(is_numeric($d) && $d > 0) {
        //if less then 3 seconds
        if($d < 3) return "right now";
        //if less then minute
        if($d < $minute) return floor($d) . " seconds ago";
        //if less then 2 minutes
        if($d < $minute * 2) return "about 1 minute ago";
        //if less then hour
        if($d < $hour) return floor($d / $minute) . " minutes ago";
        //if less then 2 hours
        if($d < $hour * 2) return "about 1 hour ago";
        //if less then day
        if($d < $day) return floor($d / $hour) . " hours ago";
        //if more then day, but less then 2 days
        if($d > $day && $d < $day * 2) return "yesterday";
        //if less then year
        if($d < $day * 365) return floor($d / $day) . " days ago";
        //else return more than a year
        return "over a year ago";
    }
  }
}
