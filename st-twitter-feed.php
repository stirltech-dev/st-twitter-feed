<?php
/*
Plugin Name: ST Twitter Feed
Plugin URI: https://stboston.com
Description: Adds template functions to retrieve and parse a Twitter feed using oAuth
Version: 0.1.0
Author: Stirling Technolgoies
Author URI: https://stboston.com
*/

 if ( !defined('ABSPATH') ) exit;


// @TODO need to do a check for wp-config credentials
// @LINK https://tommcfarlin.com/plugin-activation-message/
register_activation_hook( __FILE__, 'st_twitter_feed_activate' );

function st_twitter_feed_activate() {
  if ( !defined('TWITTER_CONSUMER_KEY') || !defined('TWITTER_CONSUMER_SECRET') || !defined('TWITTER_ACCESS_TOKEN') || !defined('TWITTER_ACCESS_TOKEN_SECRET') ) {
    add_action('admin_notices', 'st_twitter_feed_admin_notice');
    deactivate_plugins( plugin_basename( __FILE__ ) );
	}
}

function st_twitter_feed_admin_notice() {
  ?>
  <div class="notice notice-success is-dismissible">
      <p>You must define all credentials in your wp-config.php file to use this plugin.</p>
  </div>
  <?php
}

include_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';
include_once plugin_dir_path(__FILE__) . 'class-twitter-tweet.php';
include_once plugin_dir_path(__FILE__) . 'class-twitter-feed.php';
