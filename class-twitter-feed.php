<?php

namespace ST\Twitter;

use Abraham\TwitterOAuth\TwitterOAuth;
use ST\Twitter\Tweet;

class Feed {

  public $connection;

  public $username;

  public function __construct( $username ) {
    $this->username = $username;
    $this->connection = new TwitterOAuth( TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET );
  }

  public function get_tweets( $tweets_to_retrieve = 15, $custom_params = array() ) {
    $tweets = array();
    $param_defaults = array(
      "q" => $this->username,
      "count" => $tweets_to_retrieve
    );
    $params = array_merge( $param_defaults, $custom_params );
    $response = $this->connection->get("search/tweets", $params);
    foreach ( $response->statuses as $tweet ) {
      $tweets[] = new Tweet($tweet);
    }
    return $tweets;
  }

  /**
   * Magic method to simply return an unordered list of tweets
   * @return string The HTML for an unordered list of tweets
   */
  public function __toString() {
    $html = '<ul>';
      foreach( $this->get_tweets() as $tweet ) {
        $html .= '<li>' . $tweet->html . '</li>';
      }
    $html .= '</ul>';
    return $html;
  }
}
